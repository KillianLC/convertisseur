package com.example.killi.convertisseurhexabinaire;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class AccueilModel extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inutile_view);
    }

    public void GoToConvertNum(View v){
        startActivity(new Intent(this, MainActivity.class));

    }
    public void GoToConvertLettre(View v){
        startActivity(new Intent(this, ConvLettre.class));

    }
    public void GoToConvertTemps(View v){
        startActivity(new Intent(this, ConvTemps.class));

    }

}
