package com.example.killi.convertisseurhexabinaire;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ConvLettre extends AppCompatActivity {

    private Button btnConvert;
    private EditText zoneEntree;
    private TextView tv_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lettre_view);

        zoneEntree = (EditText)findViewById(R.id.edZoneText);
        btnConvert = (Button)findViewById(R.id.btnConvertir);
        tv_result = (TextView)findViewById(R.id.tv_result);


        btnConvert.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void onClick(View v) {
                convertir();
            }
        });

    }
    public void convertir(){
        int longueur = zoneEntree.getText().length();
        String bin = " ";
        String oct = " ";
        String hex = " ";

            for (int i = 0; i < longueur; i++) {

                char car = zoneEntree.getText().charAt(i);
                int carToAsc = (int) car;
                bin += Integer.toBinaryString(carToAsc);
                oct += Integer.toOctalString(carToAsc);
                hex += Integer.toHexString(carToAsc);
        }
        tv_result.setText("BIN: " + bin + "\n\n" + "OCT: " + oct + "\n\n" + "HEX: " + hex);
    }
}
