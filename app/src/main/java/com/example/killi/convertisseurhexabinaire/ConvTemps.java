package com.example.killi.convertisseurhexabinaire;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class ConvTemps extends AppCompatActivity {

    private Button btnConvert;
    private EditText zoneEntreeHeure,zoneEntreeMinute, zoneEntreeSeconde  ;
    private TextView tv_result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.temps_view);
        zoneEntreeHeure = (EditText)findViewById(R.id.edZoneTextHeure);
        zoneEntreeMinute = (EditText)findViewById(R.id.edZoneTextMin);
        zoneEntreeSeconde = (EditText)findViewById(R.id.edZoneTextSeconde);
        btnConvert = (Button)findViewById(R.id.btnConvertir);
        tv_result = (TextView)findViewById(R.id.tv_result);

        try{
            laConvertion();
        }catch (final ArithmeticException e){
            Toast.makeText(ConvTemps.this, "Vous devez remplir chaque case", Toast.LENGTH_SHORT).show();
        }
    }
    public void laConvertion(){
        btnConvert.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void onClick(View v) {
                int heure = Integer.parseInt(zoneEntreeHeure.getText().toString());
                int minute = Integer.parseInt(zoneEntreeMinute.getText().toString());
                int seconde = Integer.parseInt(zoneEntreeSeconde.getText().toString());


                int ms = heure * 3600000 + minute * 60000 + seconde * 1000;
                int sec = heure * 3600 + minute * 60 + seconde;
                tv_result.setText("Milliseconde : " + ms + "\n\n" + " Seconde: " + sec);
            }
        });
    }
}
