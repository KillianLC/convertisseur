package com.example.killi.convertisseurhexabinaire;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button btnConvert;
    private EditText zoneEntree;
    private TextView tv_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        zoneEntree = (EditText)findViewById(R.id.edZoneText);
        btnConvert = (Button)findViewById(R.id.btnConvertir);
        tv_result = (TextView)findViewById(R.id.tv_result);

        btnConvert.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void onClick(View v){
                int dec = Integer.parseInt(zoneEntree.getText().toString());
                String bin = Integer.toBinaryString(dec);
                String oct = Integer.toOctalString(dec);
                String hex = Integer.toHexString(dec);
                tv_result.setText("DEC: "+ dec +"\n\n"+ "BIN: "+ bin +"\n\n"+ "OCT: "+ oct +"\n\n"+ "HEX: "+ hex);
            }
        });
    }

}
